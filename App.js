import React from 'react';
import { View, StatusBar } from 'react-native';
import Header from './components/Header';
import CardList from './components/CardList';

export default class App extends React.Component {

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ECECEC', }}>
        <StatusBar barStyle="light-content" />
        <Header title='Dashboard' subtitle='Newcastle upon Tyne' />
        <CardList />
      </View>
    );
  }
}
