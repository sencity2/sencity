import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import Card from '../Card';

const ModuleWeather = (props) => {
  const {
    title,
    day,
    temperature,
    realFeel,
    clouds,
    label,
    labelValues,
    viewIconSmall,
    viewIconLarge,
    iconSmall,
    iconLarge,
  } = weatherStyle;

  return (
    <Card>
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <Text style={title}>Weather</Text>
            <Text style={day}>{props.day}, {props.time}</Text>
            <View style={viewIconLarge}>
              <Image
              style={iconLarge}
              source={require('../../assets/mostlyclear.png')}
              />
            </View>
            <Text style={clouds}>{props.cloudsStatus}</Text>
          </View>
          <View>
            <Text style={temperature}>{props.temperature}*C</Text>
            <Text style={realFeel}>{props.realFeel}*C</Text>
          </View>
        </View>
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={viewIconSmall}>
              <Image
              style={iconSmall}
              source={require('../../assets/precipitation.png')}
              />
            </View>
            <View style={viewIconSmall}>
              <Image
              style={iconSmall}
              source={require('../../assets/windblue.png')}
              />
            </View>
            <View style={viewIconSmall}>
              <Image
              style={iconSmall}
              source={require('../../assets/humidity.png')}
              />
            </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={labelValues}>{props.precipitation}%</Text>
            <Text style={labelValues}>{props.wind}km/h</Text>
            <Text style={labelValues}>{props.humidity}%</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={label}>Precipitation</Text>
            <Text style={label}>Wind</Text>
            <Text style={label}>Humidity</Text>
          </View>
        </View>
      </View>
    </Card>
  );
};

const windowDims = Dimensions.get('window');
const cardSize = (windowDims.width / 2) - 12;
const viewWidth = (cardSize / 3) - 2;
const viewHeight = (cardSize / 6);

const weatherStyle = StyleSheet.create({
  title: {
    fontSize: 14,
    color: '#000',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  day: {
    fontSize: 10,
    color: '#9F9F9F',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  temperature: {
    fontSize: 30,
    color: '#000',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  realFeel: {
    fontSize: 17,
    color: '#9F9F9F',
    textAlign: 'center',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  clouds: {
    fontSize: 12,
    color: '#9F9F9F',
    textAlign: 'center',
    marginBottom: 2,
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  label: {
    fontSize: 8,
    color: '#9F9F9F',
    width: viewWidth,
    textAlign: 'center',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  labelValues: {
    fontSize: 14,
    color: '#000',
    width: viewWidth,
    textAlign: 'center',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  viewIconSmall: {
    width: viewWidth,
    height: viewHeight,
    padding: 3,
    alignItems: 'center',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  iconSmall: {
    flex: 1,
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  viewIconLarge: {
    width: (cardSize / 9) * 5,
    height: (cardSize / 6) * 2,
    padding: 3,
    alignItems: 'center',
    // borderWidth: 0.5,
    // borderColor: '#000',
  },
  iconLarge: {
    flex: 1,
    width: 150,
    height: 150,
    resizeMode: 'contain',
  }
});

export default ModuleWeather;
