import React from 'react';
import {
  Text,
  StyleSheet
} from 'react-native';
import Card from '../Card';

// dane testowe ///////////////////////////////////
const dataTest = [
  {
    title: 'AirCondition',
    date: '20.08.2018',
    time: '12:20',
    clouds: 'Modulate',
    temperature: 32,
    realFeel: 36,
    windStr: 12,
    windDir: 'N',
    humidity: 45,
  },
];
// koniec danych testowych ////////////////////////

class ModuleAirCondition extends React.Component {

  render() {
    const { title, date, time, temperature } = airConditionStyle;

    return (
      <Card>
        <Text style={title}>{dataTest.title}</Text>
        <Text style={date}>{dataTest.date}</Text>
        <Text style={time}>{dataTest.time}</Text>
        <Text style={temperature}>{dataTest.temperature}</Text>
      </Card>
    );
  }
}

const airConditionStyle = StyleSheet.create({
  title: {
    fontSize: 15,
    color: '#000'
  },
  date: {
    fontSize: 10,
    color: '#000'
  },
  time: {
    fontSize: 10,
    color: '#000'
  },
  temperature: {
    fontSize: 25,
    color: '#000'
  }
});

export default ModuleAirCondition;
