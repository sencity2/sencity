import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';

const AddModule = () => (
    <View style={itemStyle.container}>
      <TouchableOpacity>
        <Image
        style={itemStyle.image}
        source={require('../../assets/addmodule.png')}
        />
      </TouchableOpacity>
    </View>
  );

const windowDims = Dimensions.get('window');
const itemSize = (windowDims.width / 2) - 12;

const itemStyle = StyleSheet.create({
  container: {
    width: itemSize,
    height: itemSize,
    alignItems: 'center',
    marginBottom: 8,
    padding: 15,
  },
  image: {
    flex: 1,
    width: 200,
    height: 200,
    resizeMode: 'contain'
  }
});

export default AddModule;
