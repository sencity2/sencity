import React from 'react';
import {
  View,
  ScrollView,
  StyleSheet
} from 'react-native';
import ModuleWeather from './modules/ModuleWeather';
import ModuleAirCondition from './modules/ModuleAirCondition';
import AddModule from './modules/AddModule';

class CardList extends React.Component {
  componentWillMount() {
    console.log('CardList loaded');
  }

  render() {
    return (
      <ScrollView>
        <View style={cardListStyle.row}>
          <ModuleWeather
            day={'tuesday'}
            time={'14:00'}
            cloudsStatus={'Cloudy'}
            temperature={28}
            realFeel={26}
            precipitation={'1'}
            wind={'18'}
            humidity={'40'}
          />
          <ModuleAirCondition />
          <AddModule />
        </View>
      </ScrollView>
    );
  }
}

const cardListStyle = StyleSheet.create({
  row: {
    // flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: 8,
    paddingBottom: 0,
  },
});

export default CardList;
