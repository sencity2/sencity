import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';

const Card = (props) => (
    <TouchableOpacity>
      <View style={cardStyle.container}>
        {props.children}
      </View>
    </TouchableOpacity>
  );

const windowDims = Dimensions.get('window');
const itemSize = (windowDims.width / 2) - 12;

const cardStyle = StyleSheet.create({
  container: {
    width: itemSize,
    height: itemSize,
    backgroundColor: '#fff',
    borderWidth: 0,
    borderColor: '#ddd',
    borderRadius: 2,
    shadowRadius: 2,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    elevation: 2,  // shadow for android
    marginBottom: 8,
    padding: 3
  }
});

export default Card;
