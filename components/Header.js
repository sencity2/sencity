import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

const Header = (props) => {
    const { container, title, subtitle } = headerStyles;

    return (
      <View style={container}>
        <Text style={title}>{props.title}</Text>
        <Text style={subtitle}>{props.subtitle}</Text>
      </View>
    );
  };

const headerStyles = StyleSheet.create({
  container: {
    backgroundColor: '#e74c3c',
    alignItems: 'center',
    justifyContent: 'center',
    height: 65,
    paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    elevation: 2,
  },
  title: {
    fontSize: 20,
    color: '#fff'
  },
  subtitle: {
    fontSize: 12,
    color: '#fff'
  }
});

export default Header;
